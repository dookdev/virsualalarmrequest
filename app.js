const express = require('express');
const http = require('http');
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser');
const cors = require('cors');
const request = require('request');
const conf = require('./node-env').config();
const xlsxFile = require('read-excel-file/node');
const _ = require('lodash');

const app = express();
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});
app.use(cors({ origin: '*' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '20mb' }));
app.use(bodyParser.json());

app.post('/api/virsualAlarmRequest', async (req, res) => {
    console.log("========req.body==========");
    console.log(req.body);
    console.log('==========================');
    xlsxFile(__dirname + '/mockup-data/AlarmDataList.xlsx', { sheet: req.body.sheet }).then(async (rows) => {
        if (rows.length > 0) {
            rows.shift();
            var dataList = [];
            if (req.body.dataSet == 'all') {
                dataList = rows;
            } else if (req.body.dataSet == 'rang') {
                dataList = _.slice(rows, req.body.startRow, req.body.endRow);
            }
            var count = 0;
            for (i in dataList) {
                var spl = dataList[i][1].split(' ');
                var dateSpl = spl[0].split('/');
                var date = dateSpl[2] + '-' + dateSpl[1] + '-' + dateSpl[0] + ' ' + spl[1];
                var dateTime = new Date(date);
                const params = {
                    "RuleName": "VAS_IT_CORRELATION_10",
                    "system": "FM",
                    "emsIp": "",
                    "manager": "VAS",
                    "domain": "EDS",
                    "neType": "Router",
                    "region": dataList[i][0],
                    "amoName": dataList[i][7],
                    "alertName": dataList[i][6],
                    "severity": dataList[i][3],
                    "description": dataList[i][8],
                    "col1": dataList[i][4],
                    "col2": dataList[i][5],
                    "col3": "",
                    "col4": dataList[i][9],
                    "col5": "",
                    "deviceIp": "",
                    "reserve1": dataList[i][10],
                    "reserve2": "",
                    "reserve3": "",
                    "reserve4": "",
                    "reserve5": "",
                    "alarmTime": (dateTime.getTime()/1000).toString(),
                    "node": "",
                    "sitecode": dataList[i][11],
                    "vendor": "3",
                    "serverserial": dataList[i][2]
                };
                try {
                    const response = await requestService(params);
                    count++;
                    if (count == dataList.length) {
                        res.json({ success: true, meesage: response });
                    }
                } catch (error) {
                    res.status(404).json(error);
                    break;
                }
            }
        }
    });
});

function requestService(params) {
    return new Promise((resolve, reject) => {
        console.log('<<<<<<<Request>>>>>');
        console.table(params);
        request(
            {
                method: 'POST',
                rejectUnauthorized: false,
                strictSSL: false,
                json: true,
                headers: { 'Content-Type': 'application/json' },
                url: conf.url,
                body: params ? params : {}
            },
            function (error, response, body) {
                // console.log("==================response========");
                // console.log(response);
                // console.log("=================================");
                // console.log("==================body============");
                // console.log(body);
                // console.log("=================================");
                // console.log("==================error===========");
                // console.log(error);
                // console.log("=================================");
                if (error) {
                    reject({ success: false, meesage: error });
                } else {
                    resolve(body);
                }
            });
    });
}

const port = conf.port;
app.set('port', port);
const server = http.createServer(app);
server.listen(port, () => console.log('Service Running at port: ' + port));
server.setTimeout(1800000);