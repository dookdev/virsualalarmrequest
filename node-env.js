const env_dev = require('./environments/dev');
const env_stage = require('./environments/stage');
const env_prod = require('./environments/prod');

exports.config = function () {
    const node_env = process.env.NODE_ENV;
    var env = {};
    switch (node_env) {
        case "development":
            env = env_dev;
            break;
        case "staging":
            env = env_stage;
            break;
        case "production":
            env = env_prod;
            break;
        default:
            env = env_dev;
            break;
    }
    console.log('***********environment************');
    console.log('******* >>> ' + (node_env ? node_env : 'development') + ' <<<< *****');
    console.log('**********************************');
    return env;
};